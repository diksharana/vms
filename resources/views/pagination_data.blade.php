@foreach($data as $row)
  <tr>
    <td>{{ $row->id }}</td>
    <td>{{ $row->name }}</td>
    <td>{{ $row->age }}</td>
    <td>{{ $row->gender }}</td>
    <td>{{ $row->phone }}</td>
    <td>{{ $row->email }}</td>
    <td>{{ $row->pincode }}</td>
    <td>{{ $row->image }}</td>
  </tr>
@endforeach

<tr>
    <td colspan="8" align="center">
        {!! $data->links() !!}
    </td>
</tr>
