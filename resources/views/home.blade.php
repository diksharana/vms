@extends('layouts.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row" id="stat_box">

          <!-- ./col -->
          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box">
              <div class="inner">
                <h3>3<sup style="font-size: 20px"></sup></h3>

                <p>Queries</p>
              </div>
              <div class="icon">
                <i class="ion ion-happy-outline"></i>
              </div>
              <a href="/admin/meetings" class="small-box-footer"><i class='fas fa-plus'></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box">
              <div class="inner">
                <h3>4</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="/admin/add_new_visitor" class="small-box-footer"><i class="fas fa-plus"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-4">
            <!-- small box -->
            <div class="small-box">
              <div class="inner">
                <h3>6</h3>

                <p>Total Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-people-outline"></i>
              </div>
              <a href="/admin/visitors" class="small-box-footer"><i class="fas fa-plus"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable" id="home_search">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                  <h4>
                      Search Visitor
                      <a href="/admin/add_new_visitor" id="home_add_visitor">Add Visitor</a>
                  </h4>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <input type="search" name="serach" id="serach" class="form-control" placeholder="Type name or other details.." />
                </div>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                  <thead>
                    <tr align="center">
                        <th>Visitor ID</th>
                        <th>Visitor Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>E-mail Address</th>
                        <th>Pincode</th>
                        <th>Photograph</th>
                    </tr>
                  </thead>
                  <tbody>
                    @include('pagination_data',['data' => $data])
                  </tbody>
                  </table>
                  <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                </div>
              </div><!-- /.card-body -->
            </div>
            <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
            <script>
              $(document).ready(function(){
                function fetch_data(page, query){
                    $.ajax({
                        url:"/home/fetch_data?page="+page+"&query="+query,
                        success:function(data){
                            $('tbody').html('');
                            $('tbody').html(data);
                        }
                    })
                }

                $(document).on('keyup', '#serach', function(){
                    var query = $('#serach').val();
                    var page = $('#hidden_page').val();
                    fetch_data(page, query);
                });

                $(document).on('click', '.pagination a', function(event){
                    event.preventDefault();
                    var page = $(this).attr('href').split('page=')[1];
                    $('#hidden_page').val(page);

                    var query = $('#serach').val();

                    $('li').removeClass('active');
                    $(this).parent().addClass('active');
                    fetch_data(page, query);
                });
              });
            </script>
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
