<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>vms</title>
    <link rel="icon" type="image/x-icon" href="images" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/css/owl.carousel.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/css/owl.theme.default.min.css') }}" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/css/styles.css') }}" rel="stylesheet" />

</head>
<body style="background-color: #edeff3;">
    <div id="landing">
        <div class="row" id="land_top">
            <div class="col-md-2">
                VMS
                <!-- <img src="{{ asset('img/images/logo-vms-1.png') }}" width="100px" /> -->
            </div>
            <div class="col-md-7">
                <ul>
                    <li><a href="#landingHead">Home</a></li>
                    <li><a href="#land_brain">Personal</a></li>
                    <li><a href="#land_solution">Life & Education</a></li>
                    <li><a href="#landingBody">Work</a></li>
                    <li><a href="#land_footer">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul>
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Sign up</a></li>
                </ul>
            </div>
        </div>
        <div id="landingHead">
            <div>
                Anurag Singh Thakur
            </div>
        </div>
        <svg id="land_svg_1" viewBox="0 0 50 50">
            <path d="M0 0 q25 15 50 0 q-15 25 0 50 q-25 -15 -50 0 q15 -25 0 -50"
             style="fill:none; stroke:#0000aa; stroke-width:3"></path>
        </svg>
        <svg width="50" height="50" id="land_svg_2">
            <path d="M3 0 l36 0 -18 33 z" fill="skyblue"></path>
        </svg>
        <div class="row" id="land_brain">
            <div class="col-md-5">
                <h1>Personal</h1>
                <p>
                    Anurag Singh Thakur (born 24 October 1974) is a member of
                    the Lower House of Parliament in India from Hamirpur in Himachal Pradesh,
                    and also serves as a Minister of State for Finance and Corporate Affairs.
                    He is the son of Prem Kumar Dhumal, the former Chief Minister of Himachal Pradesh.
                    <span id="personal_dots">...</span>
                    <span id="personal_more"> 
                        He was first elected to the Lok Sabha in May 2008 in a by poll as a candidate of 
                        the Bharatiya Janata Party.<sup>[1]</sup> He is a four time MP, being a member of 
                        14th, 15th, 16th, and 17th Lok Sabha. He was awarded the Sansad Ratna Award in 2019.
                    </span>
                </p><br />
                <button onclick="personal_read()" id="personal_button" class="btn">Know More</button>
            </div>
            <div class="col-md-7">
                <div><img src="{{ asset('img/avatar.png') }}" /></div>
            </div>
            <svg height="30" width="30"  id="land_svg_3">
                <defs>
                    <filter id="f1" x="0" y="0">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="15" />
                    </filter>
                </defs>
                <rect width="30" height="30" stroke="green" stroke-width="3" fill="#0000aa" filter="url(#f1)" />
            </svg>
        </div>
        <div class="row" id="land_solution">
            <div class="col-md-7">
                <div><img src="{{ asset('img/avatar5.png') }}"></div>
            </div>
            <div class="col-md-5">
                <h1>Early life and education</h1>
                <p>
                    Thakur was born on 24 October 1974 at Hamirpur.
                    He is the eldest son of Prem Kumar Dhumal and Sheela Devi.
                    He studied at Dayanand Model School, Jalandhar and completed
                    his BA from Doaba College, Jalandhar.
                </p><br />
                <a href="#discover-now">Know More</a>
            </div>
            <div>
                <svg id="land_svg_4" viewBox="35 15 230 120">
                    <path d="M23.948.042c-.413-.028-.817-.042-1.214-.042-8.6 0-13.497 6.557-15.278 11.833
                            l4.727 4.727c5.428-1.944 11.817-6.66 11.817-15.168 0-.44-.017-.89-.052-1.35z
                            m-11.277 14.178l-2.883-2.883
                            c1.221-2.859 4.691-8.945 12.199-9.32-.251 5.775-4.041 9.932-9.316 12.203z
                            m5.471 1.538c-.547.373-1.09.71-1.628 1.011-.187.891-.662 1.842-1.351 
                            2.652-.002-.576-.162-1.156-.443-1.738-.495.225-.966.418-1.414.588.66 
                            1.709-.012 2.971-.915 4.154 1.296-.098 2.656-.732 3.728-1.805 1.155-1.155 1.967-2.823 2.023-4.862z
                            m-11.82-6.469c-.579-.28-1.158-.438-1.732-.441.803-.681 1.744-1.153 
                            2.626-1.345.314-.552.667-1.097 1.039-1.633-2.039.055-3.708.867-4.864 2.023-1.071 
                            1.071-1.706 2.433-1.804 3.728 1.184-.904 2.446-1.576 4.155-.914.173-.471.366-.944.58-1.418z
                            m7.738.663c-.391-.391-.391-1.023 0-1.414s1.023-.391 1.414 0c.391.392.391 1.024 0 1.415
                            s-1.024.39-1.414-.001z
                            m4.949-4.951c-.78-.78-2.047-.78-2.828 0-.781.781-.781 2.049 0 2.829.781.781 
                            2.048.781 2.829 0 .78-.78.78-2.047-.001-2.829zm-1.908 1.911
                            c-.273-.273-.273-.718 0-.99.271-.273.717-.273.99 0 .272.272.271.717 0 .99-.274.272-.718.272-.99 0z
                            m-6.747 10.65c-1.492 3.81-5.803 6.208-10.354 6.438.219-4.289 2.657-8.676 6.64-10.153l.805.806
                            c-4.331 2.755-4.653 5.346-4.665 6.575 1.268-.015 4.054-.344 6.778-4.464l.796.798z"
                        style="fill: #1e266d; transform: rotate(45deg); transform-origin: 24px 0px;">
                        <animateMotion
                            path="
                                M50,75 
                                C50 140, 150 140, 150 75 
                                C150 10, 250 10, 250 75 
                                C250 140, 150 140, 150 75 
                                C150 10, 50 10, 50 75
                                C50 150, 250 150, 250 75
                                C250 0, 50 0, 50 75"
                            begin="0s" 
                            dur="50s" 
                            repeatCount="indefinite"
                            rotate="auto"
                        />
                    </path>
                </svg>
            </div>
        </div>
        <div id="landingBody">
            <h1>Work</h1>
            <div>
                <div>
                    <p>
                        <span>A</span>nurag Thakur
                        in May 2008, succeeded his father when he
                        was elected as Member of Parliament of India's 14th Lok Sabha
                        from Hamirpur constituency. He was re-elected to the 15th Lok Sabha in 2009,
                        16th Loksabha in 2014, and 17th Loksabha in 2019. Later, Thakur was appointed
                        the president of the All India Bharatiya Janata Yuva Morcha. He was
                        honoured with the Sansad Ratna Award in 2019 for outstanding performance in
                        the 16th Lok Sabha. In May 2019, Thakur became Minister of State for Finance
                        and Corporate Affairs.
                    </p>
                </div>
                <div>
                    <div class="owl-carousel owl-theme" id="owl-three">
                        <div class="item">
                            <h4>Asptal- Sansad Mobile Swasthya (SMS) Seva</h4>
                        </div>
                        <div class="item">
                            <h4>Sansad Star Khel Mahakumbh</h4>
                        </div>
                        <div class="item">
                            <h4>Sansad Bharat Darshan</h4>
                        </div>
                        <div class="item">
                            <h4>Asptal- Sansad Mobile Swasthya (SMS) Seva</h4>
                        </div>
                        <div class="item">
                            <h4>Sansad Star Khel Mahakumbh</h4>
                        </div>
                        <div class="item">
                            <h4>Sansad Bharat Darshan</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h4>Positions Held</h4>
                <ul>
                    <li>MoS Finance and Corporate Affairs, GOI</li>
                    <li>4th term Member of Parliament (Lok Sabha)</li>
                    <li>Chief Whip in 16th Lok Sabha Lieutenant in the Territorial Army</li>
                    <li>Fmr. Chairman, Parliamentary Standing Committee on IT (IT, Telecom, Information & Broadcasting, Postal)</li>
                    <li>Fmr. Member, Public Accounts Committee</li>
                    <li>Fmr. India Representative to WTO Parliamentary Steering Committee</li>
                    <li>Recipient of Sansad Ratan Award for outstanding performance in Parliament</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" id="land_footer">
        <div class="col-md-12">
            <strong>
                Copyright &copy; 2020 <a target="_blank" href="https://amakein.com/">Amakein</a>.
            </strong>
                All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.0.2
            </div>
        </div>
    </div>

    <!-- Scripts -->

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/js/scripts.js') }}"></script>
    <script>
        window.onscroll = function(){
          "use strict";
          if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
            document.getElementById('land_top').classList.add('land_top');
          } else {
            document.getElementById('land_top').classList.remove('land_top');
          }
        }
    </script>
</body>
</html>
