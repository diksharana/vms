@extends('layouts.admin')
@section('content')
  <div class="content prn" id="add_visitor">
    <h2> Edit User </h2>
    <form id="User-sell" method="post" class="dropzone" action="/edit_save_user" enctype="multipart/form-data">
      @csrf
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->name}}" name="name"  placeholder="Name" required />
        </div>
        <div class="form-group">
            <input type="number" class="form-control" value="{{$visitor->age}}" name="age" placeholder="Age" required />
        </div>
        <div class="form-group">
          <select class="form-control" value="{{$visitor->gender}}" name ="gender" required>
            <option name ="male" value= "Male"{{$visitor->gender=='Male'?'selected':''}}>Male</option>
            <option name ="female" value= "Female"{{$visitor->gender=='Female'?'selected':''}}>Female</option>
          </select>
        </div>
        <div class="form-group">
            <input type="number" class="form-control" value="{{$visitor->phone}}" name="phone" placeholder="Phone" required />
        </div>
        <div class="form-group">
            <input type="email" class="form-control" value="{{$visitor->email}}" name="email" placeholder="Email Address" required />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->father_husband_name}}" name="father_husband_name" placeholder="Father/Husband Name" required />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->occupation}}" name="occupation" placeholder="Occupation" required />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->village}}" name="village" placeholder="Village" required />
        </div>
        <div class="form-group">
          <input type="text" class="form-control" value="{{$visitor->tehsil}}" name="tehsil" placeholder="Tehsil" required />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->district}}" name="district" placeholder="District" required />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->full_address}}" name="full_address" placeholder="Complete Permanent Address" required />
        </div>
        <div class="form-group">
            <input type="number" class="form-control" value="{{$visitor->pincode}}" name="pincode" placeholder="Pincode" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$visitor->query}}" name="query" placeholder="Enter Query" required />
        </div>
        <div class="form-group w100">
          <textarea class="form-control" value="{{$visitor->description}}" name="description" placeholder="Description" required></textarea>
        </div>
        <div class="form-group">
          <select class="form-control" value="{{$visitor->status}}" name ="status" required>
            <option name ="open" value='Open' {{$visitor->status=='Open'?'selected':''}}>Open</option>
            <option name ="close" value="Closed" {{$visitor->status=='Closed'?'selected':''}}>Closed</option>
          </select>
        </div>
        <div class="form-group w100">
          <label for="User-images">Add image of User:</label>
          <input type="file" id="image-upload" class="form-control-file" name="file" enctype="multipart/form-data" multiple>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-dark">Save</button>
        </div>
    </form>
      </div>
@endsection
