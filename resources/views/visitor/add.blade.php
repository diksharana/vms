@extends('layouts.admin')
  @section('content')
    <div class="container-fluid" id="add_visitor_container">
      <div class="row" id="add_visitor">
        <div class="col-12">
          @if(session()->has('message'))
            <div class="alert alert-success side-alert">
              <div class="w3-theme-border">
                {{ session()->pull('message') }}
              </div>
            </div>
          @endif
          <h2>Enter Visitor's Details:</h2>
          <form id="User-sell" method="post" class="dropzone" action="/save_user" enctype="multipart/form-data">
            @csrf
              <div class="form-group">
                  <input type="text" class="form-control" name="name"  placeholder="Name" required />
                  <input type="number" class="form-control" name="age" placeholder="Age" required />
                  <select class="form-control" name ="gender" required>
                    <option name ="male" >Male</option>
                    <option name ="female" >Female</option>
                    <option name ="other" >Other</option>
                  </select>
              </div>
              <div class="form-group">
                  <input type="number" class="form-control" name="primary_phone" placeholder="Primary Phone Number" required />
                  <input type="number" class="form-control" name="secondary_phone" placeholder="Secondary Phone Number" />
              </div>
              <div class="form-group col-md-6">
                  <input type="email" class="form-control" name="email" placeholder="Email Address" />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="father_husband_name" placeholder="Father/Husband Name" required />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="occupation" placeholder="Occupation" required />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="designation" placeholder="Designation" required />
              </div>
              <div class="form-group col-md-6">
                  <input type="number" class="form-control" name="pincode" placeholder="Pincode" />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="state" placeholder="State" required />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="district" placeholder="District" required />
              </div>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" name="tehsil" placeholder="Tehsil" required />
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="village" placeholder="Village" required />
              </div>
              <div class="form-group col-md-6">
                <textarea type="text" rows="4" class="form-control" name="permanent_address" placeholder="Complete Permanent Address" required></textarea>
              </div>
              <div class="form-group">
                <input type="checkbox" />&nbsp;Check if the Correspondence address is same as the Permanent address.
              </div>
              <div class="form-group col-md-6">
                <textarea type="text" rows="4" class="form-control" name="correspondence_address" placeholder="Complete Correspondence Address" required></textarea>
              </div>
              <div class="form-group col-md-6">
                <select class="form-control" name ="purpose" required>
                  <option name ="#" >Purpose of visit</option>
                  <option name ="personal" >Personal</option>
                  <option name ="village" >Village</option>
                  <option name ="monetary" >Monetary</option>
                  <option name ="invitation" >Invitation</option>
                  <option name ="meeting" >Meetings</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="query" placeholder="Enter Query" required />
              </div>
              <div class="form-group col-md-6">
                <textarea class="form-control" name="description" placeholder="Description" required></textarea>
              </div>
              <div class="form-group col-md-6">
                <select class="form-control" name ="status" required>
                  <option name ="open" >Open</option>
                  <option name ="close" >Closed</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="User-images">Add image of User:</label>
                <input type="file" id="image-upload" class="form-control-file" name="file" enctype="multipart/form-data" multiple>
              </div>
              <div class="form-group col-md-6">
                <select class="form-control" name ="priority" required>
                  <option name ="#" >Priority</option>
                  <option name ="very_high" >Very High</option>
                  <option name ="high" >High</option>
                  <option name ="medium" >Medium</option>
                  <option name ="low" >Low</option>
                  <option name ="very_low" >Very Low</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <button type="submit" class="btn btn-dark">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  @endsection
