@extends('layouts.admin')
@section('content')
  <div class="content prn" id="add_visitor">
          <table class="table">
            <thead>
              <th> Visitor ID	</th>
              <th> Visitor Name	</th>
              {{-- <th> Visitor Image </th>
              <th> Visitor Age	</th>
              <th> Visitor Gender	</th> --}}
              <th> Visitor Phone	</th>
              {{-- <th> Visitor Email</th>
              <th> Visitor Father/Husband name	</th> --}}
              <th> Visitor Occupation</th>
              {{-- <th> Visitor Village	</th> --}}
              <th> Visitor District	</th>
              {{-- <th> Visitor Tehsil	</th>
              <th> Visitor Full address </th>
              <th> Visitor Pincode	</th> --}}
              <th> Visitor Query	</th>
              <th> Query Description </th>
              <th> Query Status	</th>
              <th> View </th>
            </thead>
            @foreach ($visitor as $data)
            <tr>
                <td> {{$data->id}}   </td>
                <td> {{$data->name	}} </td>
                {{-- <td> {{$data->image}} </td> --}}
                {{-- <td> {{$data->age	}} </td> --}}
                {{-- <td> {{$data->gender	}} </td> --}}
                <td> {{$data->phone	}} </td>
                {{-- <td> {{$data->email }} </td> --}}
                {{-- <td> {{$data->father_husband_name	}} </td> --}}
                <td> {{$data->occupation	}} </td>
                {{-- <td> {{$data->village	}} </td> --}}
                {{-- <td> {{$data->tehsil	}} </td> --}}
                <td> {{$data->district	}} </td>
                {{-- <td> {{$data->full_addr}} </td>
                <td> {{$data->pincode	}} </td> --}}
                <td> {{$data->query	}} </td>
                <td> {{$data->description}} </td>
                <td> {{$data->status	}} </td>
                <td>
                  <a href="/admin/open/{{$data->id}}" class="page-link" target="_blank">{{$data->name}} </a>
                </td>
            </tr>
            @endforeach
          </table>
      </div>
@endsection
