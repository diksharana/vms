<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VMS | Admin Dashboard</title>
  @include('layouts.scripts')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  @include('layouts.top_navbar')
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4">
    @include('layouts.side_navbar')
  </aside>

@yield('content')

@include('layouts.footer')
@include('layouts.js')

</body>
</html>
