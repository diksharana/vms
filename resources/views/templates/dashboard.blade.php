<div id="dashboard">
    <span>{{heading}}</span>
    <div>
        <span>Pick Date</span><br />
        <select>
            <option value="last1" >Last 1 Day</option>
            <option value="last2" >Last 2 Days</option>
            <option value="last3" >Last 3 Days</option>
            <option value="last4" >Last 4 Days</option>
            <option value="last5" >Last 5 Days</option>
            <option value="last6" >Last 6 Days</option>
            <option value="last7" >Last 7 Days</option>
        </select>
    </div>
    <div class="status">
        <div>
            <label>54</label><br />
            total visitors
        </div>
        <div>
            <label>19</label><br />
            checked in visitors
        </div>
        <div>
            <label>12</label><br />
            invitees
        </div>
    </div>
    <div class="graph">
        <label>Visitors History</label><br /><br />
        <div>
            <table>
                <thead>
                    <tr>
                        <th style="width: 60px;">S. No.</th>
                        <th style="width: 20px;">&nbsp;&nbsp;</th>
                        <th style=" text-align: center;">Name</th>
                        <th style=" text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <button type="button" name="more">More</button>
        </div>
    </div>
</div>
