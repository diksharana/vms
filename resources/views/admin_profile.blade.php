@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="margin-left: 25%;">
            <div id="login_body" align="middle">
                <div>
                    <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-responsive" alt="Admin Image">
                </div>
                <div>
                    <h2> Hi Admin! </h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
