/*jslint white:true */
/*global angular, alert */

var app = angular.module('mainApp', ['ngRoute']);

app.config(function ($routeProvider) {
    "use strict";
    $routeProvider
        .when('/dashboard', {
            templateUrl: '../../resources/views/templates/dashboard.blade.php',
            controller: 'dashboardController'
        })
        .when('/visitorLog', {
            templateUrl: '../../resources/views/templates/visitor_log.blade.php',
            controller: 'visitorLogController'
        })
        .when('/reporting', {
            templateUrl: '../../resources/views/templates/reporting.blade.php',
            controller: 'reportingController'
        })
        .when('/employee', {
            templateUrl: '../../resources/views/templates/employee.blade.php',
            controller: 'employeeController'
        })
        .when('/invitees', {
            templateUrl: '../../resources/views/templates/invitees.blade.php',
            controller: 'inviteesController'
        })
        .when('/devices', {
            templateUrl: '../../resources/views/templates/devices.blade.php',
            controller: 'devicesController'
        })
        .when('/referFriend', {
            templateUrl: '../../resources/views/templates/refer_friend.blade.php',
            controller: 'referFriendController'
        })
        .when('/settings', {
            templateUrl: '../../resources/views/templates/settings.blade.php',
            controller: 'settingsController'
        })
        .when('/help', {
            templateUrl: '../../resources/views/templates/help.blade.php',
            controller: 'helpController'
        })
        .otherwise({
            templateUrl: '../../resources/views/templates/dashboard.blade.php',
            controller: 'dashboardController'
        });
});

app.controller('dashboardController', function ($scope) {
    "use strict";
    $scope.heading = "Dashboard";
});

app.controller('visitorLogController', function ($scope) {
    "use strict";
    $scope.heading = "Visitor Log";
});

app.controller('reportingController', function ($scope) {
    "use strict";
    $scope.heading = "Reporting";
});

app.controller('employeeController', function ($scope) {
    "use strict";
    $scope.heading = "Employee";
});

app.controller('inviteesController', function ($scope) {
    "use strict";
    $scope.heading = "Invitees";
});

app.controller('devicesController', function ($scope) {
    "use strict";
    $scope.heading = "Devices";
});

app.controller('referFriendController', function ($scope) {
    "use strict";
    $scope.heading = "Refer a Friend";
});

app.controller('settingsController', function ($scope) {
    "use strict";
    $scope.heading = "Settings";
});

app.controller('helpController', function ($scope) {
    "use strict";
    $scope.heading = "Help";
});
