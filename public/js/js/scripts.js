/*jslint white:true */
/*global $ */

//--------------------------------- landing page carousel controls ------------------------

$(document).ready(function(){
    "use strict";
    var owl = $('#owl-three');
    owl.owlCarousel({
        items:3,
        loop:true,
        margin:10,
        navText:["<img src='../../img/images/arrow-back.png' width='10px'>",
                 "<img src='../../img/images/arrow-next.png' width='10px'>"],
        navContainerClass:'owl-nav1',
        dots:false,
        dotsEach:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    });
});

//----------------------------------- add visitor resizing controls --------------------------

$(document).ready(function(){
    "use strict";
    $("#topMenu").click(function(){
    $("#add_visitor").toggleClass("v_left");
  });
});

//------------------------------------ welcome page personal section controls ---------------------------

function personal_read(){
    "use strict";
    var dots = document.getElementById('personal_dots');
    var more = document.getElementById('personal_more');
    var btn = document.getElementById('personal_button');
    
    if(dots.style.display === 'none'){
        dots.style.display = 'inline';
        more.style.display = 'none';
        btn.innerHTML = 'Know More';
    }else{
        dots.style.display = 'none';
        more.style.display = 'inline';
        btn.innerHTML = 'Know Less';
    }
}

//------------------------------------ search visitor controls ---------------------------



//------------------------------------ controls ---------------------------

