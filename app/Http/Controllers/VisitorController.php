<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\visitor;

class VisitorController extends Controller
{
  public function index()
  {
      return view('visitor');
  }
  public function save_user(Request $request)
  {
    // dd($request->all());
    $visitor = visitor::create($request->all());
    return redirect()->back()->with('message','Visitor added succesfully!');
  }
  public function view_visitors()
  {
    $visitor = visitor::get();
    return view('visitor.index',compact('visitor'));
  }
  public function open_visitor(Request $request,$value)
  {
    $visitor = visitor::where('id','=',$value)->first();
    return view('visitor.view',compact('visitor'));
  }
}
