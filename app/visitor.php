<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visitor extends Model
{
  protected $fillable = [ 'name','age','gender','phone','email','father_husband_name',
  'occupation','village','tehsil','district','full_address','pincode','query',
  'description','status','image'];
}
