<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/fetch_data', 'LiveSearch@fetch_data');

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::view('/admin/add_new_visitor', 'visitor.add');
Route::view('/admin/calendar', 'calendar');
Route::view('/admin/meetings', 'meetings');
Route::view('/admin/profile', 'admin_profile');
Route::view('/admin/gallery', 'gallery');
Route::view('/admin/landing_page', 'landing_page.edit');
Route::view('/admin/messages/compose', 'mailbox.compose');
Route::post('/save_user', 'VisitorController@save_user')->name('Save_User');
Route::get('/admin/visitors', 'VisitorController@view_visitors')->name('View_Visitors');
Route::get('/admin/open/{id}', 'VisitorController@open_visitor')->name('Open_Visitors');
